---
title: "TCP基础知识(面试用)"
author: "octobered"
description: "TCP面试知识笔记分享"
date: 2020-06-21
lastmod: 2020-06-21
draft: false
categories : ["网络","面试"]
UseNCSA: true
---

# TCP基本知识 面试用笔记

> 从自己的notion笔记整理出来。大概参考了很多很多的其他的资源。
>
> 还在完善中

## TCP特性

1. 面向连接、可靠、字节流
2. 向上保证 无损、非冗余、有序
3. 头部长度是≥20字节，尽管绝大多数时候都是20字节

## TCP 三次连接

### 过程

> E可以理解为Endpoint，或者Entity也可以。在常见的场景里也可以E1=客户端，E2=服务器。

E1->E2 SYN

E2->E1 SYN+ACK

E1->E2 ACK

### 意义

1. 同步双方序列号，保证后续的正常传输（引出问题，两次就可以同步）

2. 避免资源浪费，如果第三次握手的包出现了巨大的延迟，而在此期间客户端重新发送了SYN包，会导致服务器有同时建立多个连接的风险。

3. 更进一步的，根据[RFC793](https://tools.ietf.org/html/rfc793)的描述，由于网络中存在着大量的波动，因此有可能E1因为延迟的原因，发出两个不同的SYN包，E2都正确的应答了，此时如果不进行第三次的ACK，错误的应答会导致E1和E2同步到了错误的序列号，整个连接无法正常进行。

   ### 其他方式

   **Fast Open**

   尽管在上面的*标准*的TCP握手流程中，开始阶段是不能传输数据的，因此握手的时候从开始建立连接到第一个数据包发出必然要经过1个完整的RTT。为了解决上述问题，提出了[TCP Fast Open机制](https://tools.ietf.org/html/rfc7413)。

   TCP Fast Open（简写为TFO）是一个旨在在SYN和SYN-ACK阶段就可以携带数据的机制。在使用TFO的情况下可以节省一个完整的RTT，因此可以有效的改善延迟问题。然而TFO同时也会带来一定的问题，因此系统需要处理好TFO的安全问题。

   **1. 获得TFOCookie**

   *客户端发送的数据包需要设置了CookieOpt但是没有长度，在这种情况下服务器端会发放Cookie*

   ![获取TFO Cookie](https://cdn.jsdelivr.net/gh/tyde7/octobered.com-pictures@master/img/e1b_image-20200622001606909.png)

   **2. 使用TFOCookie** 

![使用TFO Cookie](https://cdn.jsdelivr.net/gh/tyde7/octobered.com-pictures@master/img/e1c_image-20200622001627417.png)

​	**3. TFO如何解决上面的问题**

- 问题1 同步双方序列号：因为仍然会交换SYN，做ACK，因此还是可以做到同步序列号。

- 问题2 资源浪费：服务器端无法识别TFO或者拒绝识别TFO时，需要忽略TFO包。按照规范，客户端会在尝试TFO失败后，使用传统的三次握手。RFC7413中建议限制整个服务器可以启用的TFO连接数量，避免大量的**有效的TFO请求**导致服务器耗尽资源。

  > 可以参考nginx中的fastopen选项。

- 问题3 由于丢包而不协调的序列号：**上层应用需要可以处理重复的SYN包。** 根据[一篇论文](http://infocom2003.ieee-infocom.org/papers/29_04.PDF)，在T1级别的BGP网络中因为网络延迟的问题而导致的重复包问题其实很少见。然而因为没有T2级别的网络的数据，因此服务器必须可以接受同样的SYN包，同时被正确的处理这一问题。否则不开启TFO会更加合适。

  > 同样的问题在TLS和QUIC中也有体现。虽然TLS和QUIC本身都是需要加密握手的，但是因为从TLS1.3开始支持的0rtt和QUIC本身带有的0rtt导致重复的包仍然可能出现。而且开发者因为信任TLS和QUIC的安全性可能会忽略掉这个问题，而默认包是不会重复的。
  >
  > （挖坑，争取有时间做个笔记。。。

## TCP四次挥手



## 其他奇怪问题

### 序列号到达最大值要怎么办

### TCP的MSS作用




---
title: "Intel 开发优化note"
author: "octobered"
description: "Intel开发优化note"
date: 2021-10-25
lastmod: 2022-01-01
draft: false
categories : ["开发", "CPU Cache优化"]
UseNCSA: true
---

## Cache

### Cache Miss

1. Cache miss会带来很大的性能开销
2. 错误的用Cache会影响别的用户
3. 隔离的时候需要考虑Cache miss的问题

## Elimate Cache-size aliased data access (cache aliasing 问题)

// 64KB Aliased Data Access

1. Store Forwarding: 如果在对一个地址进行Store后，又从同一地址进行Load，那么在存储数据可用之前，Load将不会进行。如果一个Store后有一个Load，并且它们的地址相差4KB的倍数，那么Load就会停滞，直到Load操作完成。(部分型号)
2. 更进一步 L1/L2(MLC)的Cache冲突策略：
   1. L1: 单核最多4/8个请求 之间差为2KB整
   2. L2: 32KB, 64KB, 128KB 都可能 根据型号来决定 (CPUID)
3. 建议: 
   1. 分配内存的时候不要恰好相差64K
      1. 高频访问路径也避免恰好相差64K，比如恰好要分配一个结构体PageCache 大小为64K，建议实际上地址大小差为68K
   2. 在单个线程中，避免恰好相差64K的访问
      1. 因为L1 L2基本都不共享，所以多个处理器间不需要太注意（例如GMP模型中的P的逻辑）



### false sharing问题

数据如果非常高频，对L1优化，间隔64B

如果有被逐出L1的可能或者需要对L2优化 间隔128B(?)

## NUMA

1. Linux针对NUMA框架下内存分配的几个模式
   - strict 严格，不允许分配到除了处理器所在numa之外的节点
   - prefered 更倾向于分配到较近节点
   - interleave 随机分配（其实是round-robin）
2. Auto NUMA的策略
   1. 两种基本手段
      1. CPU跟着内存：重新调度任务到别的CPU
      2. 内存跟着CPU：复制内存页到CPU所在的Node
3. 
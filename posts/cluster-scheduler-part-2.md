---
title: "Cluster Scheduler 调度器 Part2-调度问题汇总"
author: "octobered"
description: "Hawk Mercury 对比"
date: 2021-01-06
lastmod: 2022-03-26
draft: false
categories : ["Kubernetes","调度"]
UseNCSA: true
---

# 背景

## 任务的特征

> At the very minimum, we wish that all articles about job schedulers, either real or paper design, make clear their assumptions about the workload, the permissible actions allowed by the system, and the metric that is being optimized.
>
> Dror Feitelson, 1996 [11]  

### 任务的异构性



# 调度的问题

1. [*Fully* Distributed] Probing的性能和负载相关。

   在Sparrow中，对于一个含有t个Task的Job，会向n*t(n=2)个服务器发送探测请求（这里其实叫做占位更合适） 。等到前序的任务都执行完成以后，服务器就会在仍然有未执行task的情况下，执行这个task。

   这个机制很依赖n的参数。在n=2的时候，如果服务器的负载不高，发送的2*n个探针可以很快得到回复。但是当集群本身忙碌程度上升时，t越大的Job会变得越难完全地调度出来。对于Gang Scheduling风格的Job受到的印象更大。如果将n变大，信息传递的开销又会显著提高，调度的成本随之变大。

   Hawk提出了一个实验，将1000个Job调度到15000个服务器上，950个短Job(100tasks\*100s)+50个长任务(1000tasks\*20000s)，利用率中位数86%，其中2%的服务器处于完全空闲状态。

   事实上，*personally* 真实环境中如果能达到86%已经是一个很好的指标了。然而在High Load下的问题不止利用率不足，同时还带来任务调度变慢的问题。同样因为强烈依赖*随机选出的2n服务器*这个逻辑中的运气，很多*运气不好*的任务恰好选中了忙碌的服务器节点。在上述的实验中，很大一部分短任务(70%左右)运行时间被长任务阻塞而最终提交了15000s甚至更晚才被完成。这些任务很多都被长时任务拖慢，当全部任务都是短时任务时，调度的排队情况会好很多。

   *也可以看出全分布调度，并不能解决HOL问题，当然肯定会有很大程度的改善*

2. [Monolith] 调度复杂性问题

   所有的任务在同一个调度器框架中顺序调度，资源占用和服务器心跳也在同一个框架中管理。来自用户和服务器所有的请求都汇聚到单个调度器中，单点压力过大，单点也成为用户任务和服务器的关键依赖，扩大了系统风险。

   尽管可以为了降低单体调度器的压力，减少Heartbeats和降低调度QPS。然而对于大量的短时任务的场景，天然会有着很高的调度QPS，同时短任务多也会导致资源波动频繁，及时同步信息很重要。

   一个类似的场景是Serverless

   

3. [State Shared] 冲突问题

4. [Mesos] 饥饿&死锁问题

   Mesos的Offer机制调度问题更加明显。调度缓慢甚至死锁

5. [ALL] Skewed Job

   对于资源占用倾向性明显的任务

6. 

# 调度的方法

调度的目标：

→ 高集群利用率

→ 快速调度

→ 提供合理的优先级，保证各级任务合理运行



放置策略的目标：

→ 通过放置策略来进一步提高利用率



## 基于资源的调度方法

正如Sparrow中提到的，任务时间是一个很难估计的

任务分配: Omega Borg Sparrow Mesos

### 调度策略

#### Mercury 胶水

Mercury将调度任务分为`Guaranteed`和`Queueable`两种类型。 中央调度器`Pessimistic`地调度`Guaranteed`容器，而分布式调度器负责`Queueable`. 这种英特尔粘胶水行为的解释是：为了同时拥有Monolith单体调度资源状态维护简单同时还可以保证调度速度。

但是事实上系统当中一旦存在多个调度器，就无法避免资源冲突。Mercury规定`Guaranteed`的优先级总大于`Queueable`容器(毕竟名字听起来就可以晚点)，因为排队`Queueable`被认定是安全的，因此`Queueable`发生冲突的时候，直接排队就可以了。



### 放置策略

#### MinMax 分配策略

#### DRF

DRF 是一个保证用户之间的 Fairness 的算法。

首先对于每个用户计算他们的 Dominate Resource, 

> 例如:
>
> Node: 18CPU, 36G Mem
>
> User 1已经使用资源: 1CPU, 4G 
>
> User 2已经使用资源: 8CPU, 8G
>
> 对于用户1而言，CPU 使用率占机器总 CPU 的比例是: 1/18, 内存是 4G/36G = 1/9, 内存的使用率1/9大于 CPU 的使用比例 1/18, 因此内存是用户 1 的主导资源 (Dominated Resource), 用户 1 的主导比例(Dominated Share) 是1/9
>
> 对于用户 2 而言，CPU 比例是 8/18 = 4/9, 而内存是 8/36=2/9, 因此 CPU 是主导资源，用户 2 主导比例是2/9
>
> 因为用户 2 的主导比例 2/9> 用户 1的主导比例 1/9 ，所以在下一次分配时候，为了保证资源分配的公平性，用户 1 优先得到资源。



##### *DRF的饥饿问题*

###### 任务饥饿问题 [JIRA 1086](https://issues.apache.org/jira/browse/MESOS-1086) [JIRA 1791](https://issues.apache.org/jira/browse/MESOS-1791) [JIRA 3202](https://issues.apache.org/jira/browse/MESOS-3202)

对于一些使用的很快的`User`当排序很稳定（不会随着时间变化）的时候（不引入任何随机因素） 、而资源本身持续不足、任务运行时间比较均衡的时候 会出现一部分`User`持续获得资源。

> Mesos JIRA 1086
>
> 在 Mesos 当中，Mesos 提供资源给`Framework`，这里的`Framework`类似于 User 的概念。Mesos 在0.18.0之前，按照 DRF 权重 , Framework 名字 ASCII 的顺序进行排序。
>
> 假设某一时刻一组框架 F0... Fn
>
> 1.  F0...Fn 都已经达到了相同的 DRF 权重的状态
>
> 2. 字符串排序是不会随着时间变化的，这里假设 F0>F1>....>Fn
>
> 3. 当前集群的资源最多可以满足 m 个Framework且 m<n
>
> 4. 为简化计算，对于任何一个 Framework 都会在计算 t 秒后释放当前的计算资源，并且再次需求同样的资源
>
>    我们可以看到
>
>    1. 因为1,2，DRF 排序不生效而开始按照字符串排序，因为 3 在最开始F0..m-1 可以被满足, Fm...Fn 在等待
>    2. 因为 4 在 t 秒后开始释放资源，假设会释放 p 个资源，p <= m，（任务不一定是同时开始的）
>    3. 此时系统有了 p 个资源，{F0..Fn} 中的 p 个元素, Fm...Fn 开始比较，他们因为资源都释放了，所以 DRF 权重仍然相同。而因为 2 ，此时 F0...Fp-1又会被满足。这个过程不断重复Fm...Fn将持续无法获得资源。
>
>    Mesos 的解决方法: 在排序的时候按照 DRF，分配次数，Framework 名字来排序。



##### *DRF 的碎片问题*

[Mesos-6844 Avoid offer fragmentation between multiple frameworks / within a single framework.](https://issues.apache.org/jira/browse/MESOS-6844)

[MESOS-9324 Resource fragmentation: frameworks may be starved of port resources in the presence of large number roles with quota.](https://issues.apache.org/jira/browse/MESOS-9324)

**高优 Framework 不感知低优Framework 导致的资源碎片**

> Mesos 各个 Framework 不能感知其他Framework 的存在。因此对于高优先级的 Framework ，当获取到 Offer 以后，对 Offer 的使用不能做出足够的优化。

**Offer 的碎片化**

> 同时，对于设计的不太好的 Gang Scheduler， 如果这个 Framework 不支持 Hold Offer 的机制（指先预占但是不调度任务）的时候，这个 Scheduler 将会很难等到一个完整的 Offer 来调度上去。
>
> 而如果 Framework 采取了预占机制，另一个问题就是会可能出现死锁。Framework 1 预占了 8CPUs, Framework 2 预占了 4 CPUs，他们都需要 10CPUs。其中任何一个释放掉预占，就可以让系统正常运行。

##### *Mesos的死锁问题*

> 不支持优先级倒置



##### *用户间均衡不一定是好的方案*

DRF 有一个重要的特征是 不基于 需求 ，而是基于当前使用量 来做调度。

##### *Mesos计划的数个解决方案*

Mesos 在2014 年就计划参考 Omega 提供 Optimistic 的调度，在[MESOS-1607](https://issues.apache.org/jira/browse/MESOS-1607)中提出了构建 乐观调度的初期构想：标注一些资源可以同时被分到不同的`Framework`。这个时候 Omega 已经面世，共享状态的调度器提供了大型集群调度的一个优秀的解决方案。Mesos 的开发者意识到 Mesos 本身在优先级方面的不足。

到了2016年，Mesos 开始开发`Overscription `和`Reservation`的能力来

[Design Doc: Overscription for Reservations](https://docs.google.com/document/d/1RGrkDNnfyjpOQVxk_kUFJCalNMqnFlzaMRww7j7HSKU/edit#heading=h.qa9kakriyq0d)

[Meetings Optimistic Allocators](https://docs.google.com/document/d/1B_v52zCOFcwCpqCPhgYi9h630a0NE-QM9Br0nCOZUR4/edit)



[Design Doc: Offer Starvation](https://docs.google.com/document/d/1uvTmBo_21Ul9U_mijgWyh7hE0E_yZXrFr43JIB9OCl8/edit#)

Mesos 最终的解决方案是`Random Sorter`，但是这个解决方案 2018 年才被`Mesosphere`完成。

[Mesos-8936 Implement a Random Sorter for offer allocations](https://issues.apache.org/jira/browse/MESOS-8936)



## 考虑时间的调度方法

### 时间估计方法



### 放置策略

#### GS On WTM



#### Carbyne

*依赖Deadline 提高了风险* 对于突发的节点故障适应性差

### 改善对时间的估计



## DAG分析



## 纠正方法

### 纠正队列

对于调度冲突，不悲观地在调度时立马处理。如果有足够的资源给两个任务，不同的任务经理安排在同一服务器上的两个任务可能会同时运行；先前安排在服务器上的任务可能很快完成，提前释放资源，使任何冲突的解决成为不必要的。在这些情况下，通过本地队列实现的延迟纠正机制，避免了与急切检测和解决相关的不必要的开销。纠正机制利用最新的信息不断地重新评估调度决策，并在必要时做出适当的调整。

### 负载整形

#### Mercury的方式

首先，Mercury Runtime每个一段时间来计算一个指标R，`R=Tq+Tr+Tl`

`Tq`: 排队`Queueable`容器的累计执行时间

`Tr`: 正在运行容器剩余执行时间

`Tl`: `Queueable`容器上次被成功执行的时间

分布式调度器通过心跳获得这个数据Map<node, R>，按照任意选择k个最小延迟节点来分配，来平衡集群内各个节点的压力。R偏高的节点就不会有机会被调到新的任务。



### 任务窃取

> *让空服务器咬个打火机*

由于分布式调度器不知道服务器队列的内容，他们可能最终将短任务调度在长任务后面。在一个高负载的集群中，这种事件发生的概率是相当高的。即使一个短任务是用两倍于任务的Probing来调度的，如果超过一半的Probing遇到HOL问题，那么短任务的完成时间就会受到很大影响。

[Hawk] 另一种形式的Probing

当集群负载程度升高时，Scheduler发出的Probing可能有很大的概率会落到一个忙碌的服务器上。如果90%的服务器都是超载的，那么均匀的随机探测有90%的概率会返回一个超载的服务器，并从中窃取任务。

为了避免长任务被窃取过来，最终长任务“污染”了整个集群。在窃取任务的时候，不窃取长任务，而只窃取段任务（第一组连续的短任务）

#### Kubernetes 的 [Descheduler](https://github.com/kubernetes-sigs/descheduler)





### 其他

1. 适时降级

   对于基于乐观的调度，在调度有风险(比如集群利用率高, 调度失败严重)的时候，悲观调度可以减少因为contending而rescedule的问题，改善调度速度。

2. 适时升级

   对于低优先级的任务，在排队过久的情况下，适时升级任务本身的优先级来保证任务可以正常完成，避免出现prod任务一直跑、batch任务饿死的情况，也可能同时会提高资源利用率。

3. 



# Ref

> 推荐 CS744

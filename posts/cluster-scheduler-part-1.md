---
title: "Cluster Scheduler 调度器 Part1"
author: "octobered"
description: "调度器Part1 调度器对比"
date: 2021-01-06
lastmod: 2021-05-12
draft: false
categories : ["Kubernetes","调度"]
UseNCSA: true
---

# Mesos

1. 两层调度
2. 仍然算是monolith → 调度速度
3. 调度时没有邻居信息 调度信息匮乏（拓扑相关难以实现）
4. 调度器只可见空闲资源 不易抢占



# YARN



# OMEGA

1. Transaction Based
2. 



# Apollo 

JM PN RM

JM: 部署任务

RM: 资源监视 从PN聚合信息，提供JM全局视野

PN: 处理任务



RM:

1. RM不在性能关键路径，Apollo直接上了Paxos。
   1. RM即使暂时不可用问题也不大
   2. 当任务被下发后，JM直接从Pn获取信息

2. Apollo 预测(WTM)+纠错(CM)
   1. WTM: 将要被调度的任务特征+RM信息
   2. CM: 延后纠错
   3. OS任务提高使用率



### 任务队列

1. FIFO形态的任务队列但是支持一定程度的Reorder
   1. Reorder的要求是不影响其他任务的结束时间
2. PN上存在队列→JM可以预测性地调度
   1. 通过预测PN未来状态而不是根据PN当前状态进行调度
3. PN返回任务的使用信息（CPU、内存、IO）来帮助JM优化预测
4. 因为对各个任务都有预测时间，从而导致系统可以进一步预测出 不同Request下 可以获得调度的延迟

### 预测

1. WaitTime+ExecutionTime来得出Best Choice

   1. 考虑Data Locality，可能 存在一个

      1. 里的数据近（I）

      2. 调度速度快（W）

      3. 运行速度快（R）

         中和最小的，得到为E_succ

   2. 同时考虑到P，即任务成功运行的概率

      1. 计算C=P_succE_succ+KFAIL*(1-P_succ)E_succ （建议看原文原来的公式



### 匹配

1. Greedy可能不是最优
2. 通过GS算法做稳定匹配
   1. 服务器 选 任务→保证服务器拿到的一定是最优的
   2. 最优的判断方式是：该任务更需要这个服务器（该任务在该服务器上执行最省时间）
3. 排序好之后按照“wait time”（最快调度到的）排序 进行调度
4. 一批一批进行，每批里 一个服务器只能接受一个任务
   1. 降低算法复杂度 → 但是仍然>O(server)
   2. 此处理论上应该有优化不然每次调度复杂度>O(server*task)

### 纠错






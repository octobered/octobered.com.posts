---
title: "拥塞控制各种杂记(笔记为主)"
author: "octobered"
description: "网络的拥塞控制真的好复杂"
date: 2021-06-20
lastmod: 2022-01-20
draft: false
categories : ["网络","RDMA","数据中心"]
UseNCSA: true
---

## 基本知识

1. [Little's Law](https://en.wikipedia.org/wiki/Little%27s_law)
2. 

## 几个维度

### 场景

1. 普通TCP
2. Datacenter
3. RDMA
4. RPC流控大概也能算?



### 流控方式

1. loss based 基于丢包
2. ecn mark based 基于ecn信号
3. rtt based 基于延迟



### 系统模型

每个拥塞控制算法中，认为的延迟来源

1. Loss Based 
   1. Router/Switch RED



## 各种算法

### 普通TCP流控

> simple and easy
>
> oc就完事了 谁能抢过算谁赢

#### Reno



#### Cubic(2006)
Wmin: 无丢包的最大cwnd

二分方式寻找，如果Wmid-Wmin>一个阈值，则设置为Wmin+阈值。

如果二分期间发现到了WMax也仍然不丢包，说明网络发生变化，需要重新探索Wmax。Wmax探索开始是线性增加，之后如果发现仍然没有探索到的话，增大斜率加快探

![](https://s3.us-west-2.amazonaws.com/secure.notion-static.com/451c258b-3b21-4ef9-8495-2a0df22e03a7/Untitled.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Credential=AKIAT73L2G45EIPT3X45%2F20220220%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20220220T151959Z&X-Amz-Expires=86400&X-Amz-Signature=769f65753824e59eadc947cf1d513793d80b11abf459f9ee58f462d394224edf&X-Amz-SignedHeaders=host&response-content-disposition=filename%20%3D%22Untitled.png%22&x-id=GetObject)


用三次函数模拟上述的流程。开始的启动和拥塞避免用凹轮廓，最大值探索用凸轮廓。

遇到重复ACK或者ECN通知则以beta_cubic变量来降低cwnd和ssthresh，而Wmax设置成当前的cwnd。

遇到由于timeout而确认的丢包则直接减半降低cwnd，但是ssthresh仍然降低为ssthresh*cwnd. 

没有单独的慢启动算法，采用标准慢启动

#### BBR(v1)

1. 更modern loss-based不适合现代lfn下的场景(*LFN=丢个包小问题（大雾）*) rtt-based才能看到

主动探测RTT来判断。

解决的问题：

- 丢包即为拥塞：可能只是路由器的RED策略
- 高网络负载但是不丢包：增大网络压力
- AIMD策略减少过快，引起网络波动
- 弱网环境丢包严重
- BufferFloat缓冲区膨胀问题，LossBased可能会迅速地填满各层路由器的Buffer

网络里面的测不准原理：

最小RTT的测量需要非常小的数据包，因此拿不到最大带宽。最大带宽需要填满窗口因此RTT会不精确。

BBR交替测量。ProbeBW和ProbeRTT两个阶段。

ProbeBW阶段发生RTT增加甚至丢包：认为开始发生拥塞，适当降低速率。

如果在ProbeBW中没有更新RTT，定时进行ProbeRTT获取RTT更新。

## Datacenter

> 开始变得复杂起来

### 问题

1. Incast。指的是这样一种现象：1个client向N个server同时发送请求，client必须等待N个server的应答全部到达时才能进行下一步动作。N个服务器中的多个同时向client发送应答，多个同时到达的”应答”导致交换机缓存溢出，从而丢包。这样只有等server发生TCP重传超时才能让client继续。这个现象同时损害高吞吐量和低延迟。目前对于Incast的已有研究表明，降低TCP RTO的粒度是比较有效的方案，但这并没有解决所有问题。

2. Queue Buildup TCP发送流量的“贪婪性”，可以导致网络流量的大幅振荡，因而表现在交换机队列长度的大幅振荡。在队列长度增高时，会有导致两个副作用：small flow丢包产生incast、small flow在队列中延迟较长时间（在1Gb网络中是1ms vs 12ms的区别）。

3. Buffer pressure。因为许多交换机上的缓存是在端口间共享的。因此，某端口上short flow很容易因为缺少缓存受到其他端口上的large flow的影响。

### 两种方式
RTT advantage (From timely)

- RTT直接对应了因为队列堆积引起的端到端延迟时间，而ECN无法直接得到这个延迟时间。

- ECN标记仅表示包的队列占用超过了一定的阈值。在以下情况下，ECN不能很好的反应拥塞情况
  - 具有不同优先级的多个流共享同一个链路，低优先级流可能经历较大的队列延迟时间，但它的队列占用并不一定很大（不满足ECN标记的条件）

- ECN标记描述单一交换机的行为，但拥塞可能发生在多个交换机上，ECN标记并不能很好地分清楚他们。

- RTT甚至适用于支持FCoE的无损网络结构。在这种结构下，由于确保零丢包的PFC机制，使得队列占用无法反映拥塞情况。



ECN advantage

- RTT延迟本身来自出口，会出现响应震荡
  
- （ps: *不合适的RED AQM也会引起震荡...*
  
- 完全基于RTT的调度系统不收敛

  - > [对于拥有稳定带宽的拥塞控制机制，吞吐量R是延迟d和差值反馈p的函数，R=f(d,p)，如果p基于端到端的延迟测量，那再公平性和固定的延迟之间，两者只能选择一个。](http://yibozhu.com/doc/ecndelay-conext16.pdf) 

#### Timely




#### DCTCP







### 新的问题

#### [PFC死锁](https://zhuanlan.zhihu.com/p/409239714)











> 额外资料
>
> 1. CLOS Networking  https://www.cnblogs.com/jmilkfan-fanguiju/p/11825042.html
> 2. 

### RDMA









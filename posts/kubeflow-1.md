---
title: "Kubeflow(1): 背景知识"
author: "octobered"
description: "Kubeflow框架学习"
date: 2022-05-01
lastmod: 2022-05-01
draft: false
categories : ["云原生", "Kubeflow", "人工智能"]
UseNCSA: true
---
# Kubeflow背景知识

## 分布式训练

### 为什么需要分布式训练

模型越来越大。对于CV领域，腾讯在2018年用分布式训练将ResNet50训练时间压缩到了6.6分钟。而NLP领域中，GPT-2和GPT-3两个超大模型更加依赖分布式训练，单机训练事实上不可行。

### 分布式训练的思路

- 数据并行(Data Parallelism)
- 模型并行(Model Parallelism)
  
在**数据并行**中，将**样本数据**进行切分，切分后的数据被送至各个训练节点，与完整的模型进行运算，最后将多个节点的信息进行合并。

在**模型并行**中，将**模型**进行切分，完整的数据被送至各个训练节点，与切分后的模型进行运算，最后将多个节点的运算结果合并.

## Kubeflow训练

### Kubeflow支持的框架

Kubeflow通过设计不同框架的Operator来实现支持。目前有：
- tensorflow operator


### Tensorflow Operator

一个Tensorflow的训练任务需要以下部分：
1. Chief: 对训练过程的调度，做checkpoint等操作
2. Parameter Servers: 给模型参数提供一个分布式存储
3. Worker: 负责训练模型的计算
4. Evaluator: 负责计算模型的指标
    
### Katib 超参数调整

**超参数**指的是训练过程中 **不能被训练优化**的参数，比如学习率、神经网络的隐藏层的数量等。超参数是用来调节整个网络训练过程的。超参数并不直接参与到训练的过程中，他们只是配置变量。在训练过程中，参数是会更新的而超参数往往都是**恒定**的。

举例几个会被用来调整的超参数：
1. 优化器
2. 迭代次数
3. 激活函数
4. 学习率

## KFServing (Kubeflow提供在线服务)

> 在KFServing之前提供在线服务的三种方式
> 
> 1. IBM: Serverless ML on KNative
> 2. Google: TF HTTP API
> 3. MS: Azure ML Stack on Kubernetes
> 不同方式之间较为分裂，API层的规范也不一致，因此联合搞出了kfserving
KFServing1.0的目标是: 

1. Serverless ML 推理
2. 小流量发布
3. 模型解释
4. 统一的预处理&后处理

因此在这个基础上提供了以下的能力

1. 推理服务: 管理模型的生命周期
2. 配置：配置模型的部署
3. 版本：对模型不同版本做Snapshot
4. 路由：提供Endpoint和网络流量的支持

![图 5](https://res.cloudinary.com/octobered/image/upload/v1663495133/markdown/8500f3345bc9a7e1aac9d11ece713ee7f9f66f02c4f38d060433a603ce3aaaa0.jpg)  

支持的终端服务:

- Tensorflow
- PyTorch
- XGBoost...

支持的存储:

- AWS/S3标准
- GCS
- Azure Blob

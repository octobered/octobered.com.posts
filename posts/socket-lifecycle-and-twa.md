---
title: "Socket, TIME_WAIT 问题和他的各种鸡毛"
author: "octobered"
description: "Linux内Socket的TCB的结构，TIME_WAIT"
date: 2020-06-23
lastmod: 2020-06-23
draft: false
categories : ["网络","面试"]
UseNCSA: true
---

# TIME_WAIT 问题和他的各种鸡毛

## TIME_WAIT问题

`TIME_WAIT`本身是一个绝对正常的状态，在RFC793中有描述TIME_WAIT状态最主要是为了避免任何杂散数据包到达本地地址，RFC7323中做了更进一步的总结：

- 为了实现更为可靠的TCP连接关闭

  - 事实上在RFC7323中，也提到了用MSL来决定是否延迟关闭并没有很合适，尤其是考虑到很多机器开始支持tcp timestamp从而可以*一定程度地*测量出RTT，而RTT显然是一个更好的指标。但是在RFC7323中，这样描述的：

    > ```rst
    > Although there is no formal upper bound on RTT, common network
    > engineering practice makes an RTT greater than 1 minute very
    > unlikely.  Thus, the 4-minute delay in TIME-WAIT state works
    > satisfactorily to provide a reliable full-duplex TCP close.
    > Note again that this is independent of MSL enforcement and
    > network speed.
    > ```

    just take it.

- 为了保证旧的重复的数据报文被正确的消耗

但是同时保持一个`TIME_WAIT`会耗费一定的内存空间 ~~(根据[Linux内核的文档](https://github.com/torvalds/linux/blob/master/Documentation/networking/ip-sysctl.rst)，每个处在Orphan状态的套接字需要消耗64KB的**不可交换(unswappable)**的内存.)。~~

`TIME_WAIT`本身通过内核中的`Death row`进行管理.

### 避免TIME_WAIT过多的方法

1. 打开tcp_timestamps (from rfc7323 (Obsoletes rfc1323)、tcp_tw_recycle和tcp_tw_reuse。

   - 原理

     RFC1323 提供了一组 TCP 扩展，以提高高带宽路径的性能。其中一个重要的扩展就是时间戳`TSopt`。通过时间戳机制内核可以更可靠地辨认数据包是『新』的还是『过期』的。

     > 事实上在时间戳在RFC7323推出后才得以完善。在RFC1323中，并没有明确指出如果在协商了时间戳以后，新的数据包部分性地或者全部都不提供时间戳的话要怎么处理。
     >
     > 而在2014年写出的RFC7323中，明确地指出了如果一个非RST数据包收到了一个没有时间戳的数据包时，要安静地忽略掉(*Silently drop*)这个数据包。同时RFC7323也指出了这样的设计在一些极端的网络环境中导致出现无响应的连接（即可以正确地协商TSopt但是后续传输会丢失的情况）。

   - 『虚假』的tcp_tw_recycle

     ​	tcp_tw_recycle, 推测是在2.2或者2.3的某个版本引入的特性。

   - 安全可靠？

     

2. 设置一定程度的linger。（而尽可能的避免0值）

   - 设置了Linger之后，会有最多`linger`值对应的时间停留在`CLOSE_WAIT`状态。在超过该时间或者所有的包已经正常的被另一方接受(主动发起的一方，很多时候会叫做客户端)后，自然的过渡到`TIME_WAIT`状态.
   - 只有很少见的场合下需要进入`linger(0)`的状态。一个情况是如果要重启一个持有了大量TCP连接的应用，明确地知道所有的TCP连接需要被立马关闭(`RST`方式而不是`FIN`方式的`orderly release`)。因此在为了避免大量的`TIME_WAIT`导致的资源消耗的情况下，可以用`linger(0)`来保证快速释放资源。
   - 只有在`linger(0)`的时候才会用`RST`的方式释放资源，其他时候都是在用`FIN`的方式有序地释放。

3. 


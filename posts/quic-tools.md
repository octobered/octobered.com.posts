---
title: "QUIC小工具"
author: "octobered"
description: "测试QUIC连接情况的小工具"
date: 2020-06-11
lastmod: 2020-06-11
draft: false
categories : ["小工具们",]
UseNCSA: true
---



# QUIC的小工具

1. https://github.com/marten-seemann/quic-network-simulator 一个基于ns3的quic测试工具

2. https://interop.seemann.io/ 定期生成各个QUIC版本之间的兼容情况，并且进行速度测试。点击记录可以看到日志。

   1. 测试方式：上面1中的基于ns3的quic测试工具
   2. G: 没有其他干扰的数据 C: 和基于Reno的TCP连接在同一个网络里竞争速度。
   3. 目前没看到作者解释关于版本的问题，对于很多快速更换握手版本的quic库来说可能会和更换比较慢的库产生不兼容。

3. quic-go提供的example client [GitHub地址](https://github.com/lucas-clemente/quic-go/tree/master/example) 可以通过go build得到一个方便的client和echo server。**注意quic版本在不同的branch中。有特定需要的话一定要换着clone。**

4. curl的quic兼容版本 [curl提供的GitHub编译教程](https://github.com/curl/curl/blob/master/docs/HTTP3.md#quiche-version) 在MacOS上CloudFlare提供了homebrew的版本

   ```bash
   brew install --HEAD -s https://raw.githubusercontent.com/cloudflare/homebrew-cloudflare/master/curl.rb
   ```

   


---
title: "LeetCode解题记录"
author: "octobered"
description: "Leetcode解题记录"
date: 2020-06-21
lastmod: 2020-06-21
draft: false
categories : ["leetcode",]
UseNCSA: false
---

## Leetcode #76 Minimum Window Substring

```python
class Solution:
    def minWindow(self, s: str, t: str) -> str:
        l, L, R, missed = 0, 0, 0, len(t)
        required = collections.Counter(t)
        for r, c in enumerate(s, start=1):
            missed -= (required[c] > 0)
            if c in required: required[c] -= 1
            if not missed:
                while l < r and ((s[l] not in required) or (required[s[l]] < 0)):
                    if s[l] in required: required[s[l]] += 1
                    l += 1
                if R == 0 or r - l < R - L:
                    L, R = l, r
        return s[L:R]
```



注意，可以证明如果构建`required`数组，然后`required`数组在遍历时候遇到了任何一个元素的值上升为0，则说明已经match。





## Leetcode #11 [Container With Most Water](https://leetcode.com/problems/container-with-most-water/)

常规的双指针方法在这里忽略，主要是从Leetcode 84延伸的单调栈方法需要特别注意。这里首先要对数据做特殊的处理才可以沿用单调栈的方法。

参考[Leetcode上的一个讨论](https://leetcode.com/problems/container-with-most-water/discuss/385821/O(n)-time-but-O(n)-space%3A-Trapping-Rain-Water-%2B-Largest-Rectangle-in-Histogram)

Java版本的代码备份.

```java
public int maxArea(int[] height) {
        int n = height.length;
        int[] water = new int[n - 1];
        int L = 0, R = n - 1;
        int leftMax = 0;
        int rightMax = 0;
        while(L < R){//O(n)
            leftMax = Math.max(leftMax, height[L]);
            rightMax = Math.max(rightMax, height[R]);
            if(leftMax < rightMax){
               water[L] = leftMax;
               L++;
            }else{
               water[R - 1] = rightMax;
               R--; 
            }
        }
        int maxWater = largestRectangleArea(water);
        return maxWater;
    }
    
    public int largestRectangleArea(int[] height) {//O(n)
        // write your code here
        if(height == null || height.length == 0){
           return 0;
        }
        int maxRect = 0;
        Stack<Integer> stack = new Stack<Integer>();
        for(int i = 0; i <= height.length; i++){
            int curt = i == height.length ? 0 : height[i];
            while(!stack.isEmpty() && curt <= height[stack.peek()]){
                int indexPop = stack.pop();
                int h = height[indexPop];
                int w = stack.isEmpty() ? i : i - 1 - stack.peek();
                maxRect = Math.max(maxRect, w*h);
            }
            stack.push(i);
        }
        return maxRect;
    }
```




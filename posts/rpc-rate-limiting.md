---
title: "微服务RPC流控笔记"
author: "octobered"
description: "Linux内Socket的TCB的结构，TIME_WAIT"
date: 2021-12-23
lastmod: 2021-12-23
draft: false
categories : ["RPC", "流控"]
UseNCSA: true

---

# RPC流控笔记

## 背景

## DAGOR(微信)

-   由于发送到微信后端的服务请求没有单一的入口点，因此传统的全局入口点（网关）集中负载监控方法并不适用。
-   特定请求的服务调用图可能依赖于特定于请求的数据和服务参数，即使对于相同类型的请求也是如此。因此，当特定服务出现过载时，很难确定应该限制哪些类型的请求以缓解这种情况。
-   过多的请求中止浪费了计算资源，并由于高延迟而影响了用户体验。
-   由于服务 DAG 极其复杂，而且在不断演化，导致有效的跨服务协调的维护成本和系统开销过高。



## BreakWater(MIT)

1. 假定微秒级别服务，需要尽可能低的协调时间
2. 服务时间可变性，预测长尾请求
3. 服务量可变性 请求存在高峰和低谷
4. 大量客户下难协调，容易突发产生大量请求



## 关键问题

### 繁忙程度测量

1. CPU占用 → 对于部分资源不够精确，例如依赖硬盘的资源。不能区分是占用满了还是刚好
2. 队列长度 → RPC服务时间方差大，不够可靠
3. 请求延迟 → 不能确定到底是本层RPC导致还是下游导致 (from dagor)
4. 排队延迟较为精确 → 如何测量排队延迟
   1. DAGOR: 
   2. BreakWater: 最头部的包等待了多久
      1. 需要保证是Dispatcher模型 避免HOL



### 流控

#### BreakWater

Server分发Credit Client根据Credit请求

Credit 加性扩容，乘性缩容（注：*这很Reno*）

带来问题

1. Credit可能不被充分使用 → Overcommitment （注：*这也能OC?*） 过度下发，但是保持在Server能处理的Total Credit限度内
2. Credit分发额外带来消耗 → 分发随着请求
3. SLO保证：AQM丢包 只能算是兜底，实验中AQM触发频率不高



#### DAGOR

两层优先级 (Business+User)

1. Business根据业务来定，比如支付会很高
2. User根据用户ID+时间来定 
   1. User层级的优先级可以更细粒度地进行流控，不至于直接将一整个Business Priorty Level的请求全都抛弃
   2. 时间要以小时/天级别为周期，同时不能用SessionID（会因为重复登录而被刷新），避免出现降级的时候一部分用户持续可以完成请求（注：*小时级别感觉不是很够*）
3. 根据直方图进行流控，按照优先级统计流量并生成直方图，根据直方图来移动当前接受的最低优先级，更低的请求Drop 
4. 本层的限流信息会随着请求向下游流动

### Ref

1. [DAGOR infoq](https://www.infoq.cn/article/bavev*b7gth123tlwydr)
2. ZHOU H, WANG Y, GU R等, 2018. Overload control for scaling wechat microservices[J]. SoCC 2018 - Proceedings of the 2018 ACM Symposium on Cloud Computing, (December): 149–161. DOI:10.1145/3267809.3267823.
3. CHO I, SAEED A, FRIED J等, 2020. Overload Control for µs-scale RPCs with Breakwater This paper is included in the Proceedings of the Design and Implementation[J]. Osdi.


---
title: "Typora粘贴图片自动上传github获得jsdelivr地址"
author: "octobered"
description: "自动上传本地/剪贴板图片到github并且转换为jsdelivr数据"
date: 2020-06-10
lastmod: 2020-06-10
keywords: ["typora","github","linux"]
categories: ["小工具们",]
draft: true
---

# Typora更方便的插图功能——图片自动上传

> **Windows用户一定要注意脚本里的内容，**

## Typora更新

[Typora 0.9.84](https://support.typora.io/What's-New-0.9.84/) 中增加了(扩展了)跨平台的`Upload Images`功能。                       

之前的版本中，根据文档之前的自动上传只支持iPic。而现在iPic作者表示目前iPic会以维护为主，同时iPic只有Mac OS的[AppStore](https://apps.apple.com/app/id1101244278?ls=1&mt=12)上提供，而没有跨平台实现。因此其他平台的用户之前难以使用到这个非常方便的功能。

新版本的下支持

- [uPic](https://github.com/gee1k/uPic)
- PicGo-core
- PicGo.app

其中只有PicGo-core是跨平台的，可以同时支持Linux Windows等平台。

## 主要思路

通过PicGo把图片上传到GitHub指定的位置，返回基于jsdelivr的地址，利用上CDN加速。

> Jsdelivr是一个由网宿运营的CDN网站。通过使用Jsdelivr可以显著改善中国大陆访问GitHub资源的体验。



## 配置PicGo-core

1. 安装[nodejs](https://nodejs.org/)

   > 1. 大陆地区镜像：https://npm.taobao.org/mirrors/node
   >
   > 2. 可以用nvm配置，但是下面有注意事项

2. 安装picgo-core

   ```bash
   npm install picgo -g    #yarn用户可以用yarn global add picgo
   ```

   

3. 具体配置方法参考[官方文档](https://picgo.github.io/PicGo-Core-Doc/zh/guide/config.html#picbed-github)，大概格式如下。

   ```json
   {
     "repo": "", // 仓库名，格式是 username/reponame
     "token": "", // github token
     "path": "", // 自定义存储路径，比如 img/
     "customUrl": "", // 自定义域名，注意要加 http://或者 https://
     "branch": "" // 分支名，默认是 master
   }
   ```

   保存上面的文件到:

   - linux 和 macOS 均为`~/.picgo/config.json`。

   - Windows 则为`C:\Users\你的用户名\.picgo\config.json`

   4. 任意位置下保存这个脚本：
   
      ```python
      #!/usr/bin/env python3
      debug = False
      basedir = "~"
      import os, sys, subprocess, shutil
      epoch = 1
      try:
          with open(basedir+"/.picgo-github.epoch","r") as f:
              epoch = int(f.read())
      except FileNotFoundError:
          pass
      epochstr = "e%s_" % hex(epoch)[2:]
      for _filename in sys.argv[1:]:
          splitindex = _filename.rindex("/")+1
          filename = epochstr+_filename[splitindex:]
          shutil.copyfile(_filename,"/tmp/%s" % filename) # Windows用户替换temp文件夹
          if debug:
              print(filename)
          p = subprocess.Popen(["/usr/local/bin/picgo","upload","/tmp/%s" % filename], stdout=subprocess.PIPE, stderr=subprocess.PIPE) # 如果需要代理，替换下一行的<代理地址>为
          stdout = p.communicate()[0]
          if debug:
              print("---stdout---")
              print(stdout)
              print(p.returncode)
              print("---finish---")
          if "ERROR" in stdout.decode():
              print("UPLOAD FAILED!")
          else:
              print("https://cdn.jsdelivr.net/gh/<github username>/<repo name>@master/img/%s" % filename)
      
      with open(basedir+"/.picgo-github.epoch","w") as f:
          f.write(str(epoch+1))
      ```
      
      

三个需要自助修改的地方:

1. 第三行的basedir，自己任意指定，可以直接指定Home目录
   - MacOS用户: `/Users/<你的用户名>/`
   - Linux用户:`/home/<你的用户名>/` (不同发行版可能有变化)
   - Windows用户: `C:\\Users\\你的用户名\\`
2. `# Windows用户替换temp文件夹` 注释处: (Windows用户) 运行输入`%TEMP%`，然后把打开的窗口的文件夹路径替换掉前面的`/tmp`，保留`%s`
3. 



![Typora新增的选项](https://cdn.jsdelivr.net/gh/tyde7/octobered.com-pictures@master/img/e17_image-20200610150450721.png)

**这里使用picgo-github.sh应该是picgo-github.py(图里的框框里的内容修改了就好)，或者可以和我一样啰嗦地再写一个picgo-github.sh启动他。**



接下来在typora中粘贴就可以看到自动编程Jsdelivr网址啦
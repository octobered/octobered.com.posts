---
title: "Spotify Kubeflow实践" 
author: "octobered" 
description: "保量算法note" 
date: 2022-10-01 lastmod: 2022-10-11
draft: false 
categories : ["推荐","保量","SIGKDD"] 
UseNCSA: false 
---
# Spotify 从自研到KubeFlow

## 开始的形态: Scala自研

![1代](https://res.cloudinary.com/octobered/image/upload/v1665848763/markdown/c970e475de3929def1d2f086f525dd5d064f3f078d50b39779be1a663c9aa0b9.png)   
Spotify在最开始大量使用Scala语言，使用基于 Apache Beam 构建的开源数据处理库。 为了支持从数据工程到机器学习的过渡，构建了诸如用于特征工程的 Featran 库、用于模型评估的 Noether 库以及一个名为 Zoltar 的库，用于将经过训练的模型与基于 JVM 的库 Apollo 连接起来，该库几乎专门用于 Spotify 的生产服务。

Spotify开始推广一整套面向ML的产品和配置，用推荐的基础设施部署端到端机器学习解决方案来实现ML的需求，叫做Paved Road，类似内部的最佳实践。越来越多的用户在使用这一套严重依赖Scala的实践的时候，一些问题开始出现了：

1. Scala和Python两种语言需要频繁切换
2. 对于数据科学家们，Scala要比Python难用的多，大多数数据科学家都有着Python的使用经验
3. 特征的版本、配置、模型存在很多个版本，很难保证各自版本能关联上

## 第二次迭代: TFX
![2代](https://res.cloudinary.com/octobered/image/upload/v1665848730/markdown/f188a4435e8504cecc0cbf13a5b5c9837aa473ce3c4349076ca8a2e3bbda01f9.png)

2017年谷歌在KDD2017上公布了TFX，尽管在公布的时候TFX基本上就是一个内部工具，但是一些基础部分，比如`TFRecord`, `tf.Example`是可用的。Spotify此时开始将整个内部架构向TFX做初步的迁移。
迁移的第一步是标准化 ML 工作流程中步骤之间的存储格式——本质上是就步骤之间的接口达成一致，以便可以为不同 Spotify 工作负载使用的通用工具构建连接器/桥接器。Spotify的工程师们通过ML 工作流工具和内部库快速构建了连接器，这些连接器支持将这些数据和消息格式链接到 XGBoost、Tensorflow 和 scikit-learn 等常见训练库。
使用 TFRecord/tf.Example 的早期优势是对 Tensorflow DataValidation的支持，TFDV使得ML工程师可以在模型开发期间更好地理解他们的数据，并且检测生产管道和服务中的错误。
此后不久，TFX 团队发布了更多内部工具，包括 Tensorflow Transform、Tensorflow 模型分析和 Tensorflow Serving 等组件。这些工具相比Spotify内部工具有着类似的性能，同时提供了一些内部还没有的功能。尽管在那个时候TFX还没有完全开源，Spotify内部决定进一步地向TFX迁移。
这个时候的不足：
1. 缺乏一个工具把各种工具绑定在一起、提供端到端的一致体验
2. 跟踪不同的ML实验仍然需要大量的手动操作
3. Spotify内部的编排框架Luigi是面向数据通道的，对于机器学习工作流有一些不适用

## 第三次迭代: Kubeflow
![图 4](https://res.cloudinary.com/octobered/image/upload/v1665849550/markdown/0dd9d8cb0d870172cef2b8ddb290310cd4dfc09db9be38738ac4de61f9430cc6.png)  
这次Spotify切换到了Kubeflow Pipelines, 工作流组件变成了Docker容器，Kubernetes处理编排。因为第二次迭代中，已经开始使用TFX组件，因此对于用户来讲体验区别不大。
同时KFP支持共享组件，减少了团队之间造轮子的问题。同时KFP SDK支持编写参数化的Pipeline，模型在更好迭代的同时也会自动通过metadata store跟踪性能。同时在这个基础上统一化集群部署、各个团队共用服务器资源。
> Spotify同时提出了KFP和TFX之间的一些兼容问题，但是都不是很大的问题，此处就交给两个团队继续迭代优化吧